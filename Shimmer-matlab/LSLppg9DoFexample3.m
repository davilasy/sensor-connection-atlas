function void = LSLppg9DoFexample3(comPort, fileName, Stp_fnc) 
%PPGTOHEARTRATEEXAMPLE - Heart Rate from Photo Plethysmograph signal
%
%  PPGTOHEARTRATE9DOFEXAMPLE(COMPORT, PPGCHANNELNUM, CAPTUREDURATION, FILENAME)
%  plots Photo Plethysmograph and estimated Heart Rate from the Shimmer
%  paired with COMPORT. The function will stream data for a fixed duration
%  of time defined by the constant CAPTUREDURATION. The function also
%  writes the data in a tab delimited format to the file defined in
%  FILENAME.
%
%  SYNOPSIS: LSLppg9DoFexample2(comPort, PPGChannelNum, captureDuration,
%  fileName)
%
%  INPUT: comPort - String value defining the COM port number for Shimmer
%
%  INPUT: captureDuration - Numerical value defining the period of time
%                           (in seconds) for which the function will stream
%                           data from  the Shimmers.
%  INPUT : fileName - String value defining the name of the file that data
%                     is written to in a comma delimited format.
%  OUTPUT: none
%
%  EXAMPLE: LSLppg9DoFexample2('5', 13, 30, 'testdata.dat')
%
%  See also twoshimmerexample plotandwriteexample ShimmerHandleClass
%
% NOTE: To use the Java Shimmer Biophysical Processing Library in   
% conjunction with the MATLAB ID:
% Save the ShimmerBiophysicalProcessingLibrary_Rev_X_Y.jar file to
% C:\Program\Files\MATLAB\R2013b\java\jar (or the equivalent) on your PC and
% add the location of the ShimmerBiophysicalProcessingLibrary_Rev_X_Y.jar file
% to the JAVA dynamic class path:
%
% javaclasspath('C:\Program
% Files\MATLAB\R2013b\java\jar\ShimmerBiophysicalProcessingLibrary_Rev_X_Y.jar')
%
% NOTE: In this example the PPG data is pre-filtered using a second order
% Chebyshev LPF with corner freq 5Hz by using FilterClass.m
% 
% NOTE: If heartRate < 30 or heartRate > 215 or standard deviation of last
% X interbeat intervals > 100 (X = numberOfBeatsToAve) then -1 is returned.

addpath('./Resources/')                                                    % directory containing supporting functions
javaclasspath('./ShimmerBiophysicalProcessingLibrary_Rev_0_10.jar')        % Java Shimmer Biophysical Processing Library

disp('Loading library...');
lib = lsl_loadlib();

%% definitions
shimmer = ShimmerHandleClass(comPort);                                     % Define shimmer as a ShimmerHandle Class instance with comPort1
SensorMacros = SetEnabledSensorsMacrosClass;                               % assign user friendly macros for setenabledsensors

PPGChannelNum = 13;
fs = 128; %204.8                                                               % sample rate in [Hz] 
firsttime = true;

% Note: these constants are only relevant to this examplescript and are not used
% by the ShimmerHandle Class
NO_SAMPLES_IN_PLOT = 1000;                                                  % Number of samples that will be displayed in the plot at any one time
DELAY_PERIOD = 0.2;                                                        % A delay period of time in seconds between data read operations
numSamples = 0;


%% filter settings
fclp = 5;                                                                  % corner frequency lowpassfilter [Hz]; 
nPoles = 2;                                                                % number of poles (HPF, LPF)
pbRipple = 0.5;                                                            % pass band ripple (%)
lpfPPG = FilterClass(FilterClass.LPF,fs,fclp,nPoles,pbRipple);             % lowpass filters for PPG channel
% 
%% PPG2HR settings
numberOfBeatsToAve = 1;                                                    % the number of consecutive heart beats that are averaged to calculate the heart rate. Instantaneous heart rate is calculated after each detected pulse. So the last X instantaneous heart rates are averaged to give the output (where X is numberOfBeatsToAve)) must be >= 1.
useLastEstimate = 1;                                                       % true for repeating last valid estimate when invalid data is detected.
PPG2HR = com.shimmerresearch.biophysicalprocessing.PPGtoHRAlgorithm(fs,numberOfBeatsToAve,useLastEstimate);  % create PPG to Heart Rate object: Sampling Rate = 204.8Hz, Number of Beats to Average = 1 (minimum), Repeat last valid estimate when invalid data is detected.

%%
if (nargin ~=3)
    fprintf('Number of input arguments needs to be 3.\n');
    fprintf('See ''help ppgtoheartrateexample''.\n');
elseif (shimmer.connect)                                                       % TRUE if the shimmer connects
    
    % define settings for shimmer
    shimmer.setsamplingrate(fs);                                           % set the shimmer sampling rate
    PPGChannel = ['INT A' num2str(PPGChannelNum)];        
    shimmer.disableallsensors;                                             % disable all sensors
    shimmer.setenabledsensors(PPGChannel,1,SensorMacros.ACCEL,1,...        % enable PPG Channel, accelerometer, magnetometer, gyroscope and battery voltage monitor
        SensorMacros.MAG,1,SensorMacros.GYRO,1);
    shimmer.setinternalexppower(1);                                        % set internal expansion power
    shimmer.setaccelrange(0);                                              % Set the accelerometer range to 0 (+/- 1.5g) for Shimmer2r
    
    %LSL 
    % make a new stream outlet
    disp('Creating a new streaminfo...');
    info = lsl_streaminfo(lib,'Shimmer','MoCap',13,...                        % Number of channels is 11 counting the timestamp stream
        fs,'cf_float32','Shimmer3-5E79');                                   % The name SourceId will need to be changed for each device
    
    disp('Opening an outlet...');
    outlet = lsl_outlet(info);
        
    if (shimmer.start)                                                     % TRUE if the shimmer starts streaming

        plotData = [];                                               
        timeStamp = [];
        filteredplotData = [];
        heartRate = [];
        storeData = [];
        
        h.figure1=figure('Name','Shimmer PPG and movement signals');     % create a handle to figure for plotting data from shimmer
        set(h.figure1, 'Position', [100, 500, 800, 400]);
        
%         h.figure2=figure('Name','Shimmer 9DoF signals');                      % Create a handle to figure for plotting data from shimmer
%         set(h.figure2, 'Position', [100, 500, 800, 400]);
        
        elapsedTime = 0;                                                   % reset to 0    
        tic;                                                               % start timer
        
        % send data into the outlet
        disp('Now transmitting chunked data...');
        while true%(elapsedTime < captureDuration)            
            if (Stp_fnc == 1)          
                pause(DELAY_PERIOD);                                           % pause for this period of time on each iteration to allow data to arrive in the buffer

                [newData,signalNameArray,signalFormatArray,signalUnitArray] = shimmer.getdata('c');   % Read the latest data from shimmer data buffer, signalFormatArray defines the format of the data and signalUnitArray the unit
                %Data stream is obtained in the following order: Time Stamp, Low Noise Accelerometer X, 
                %Low Noise Accelerometer Y, Low Noise Accelerometer Z, Internal ADC A13, Gyroscope X, 
                %Gyroscope Y, Gyroscope Z, Magnetometer X, Magnetometer Y, Magnetometer Z

                if (firsttime==true && isempty(newData)~=1)
                    signalNameArray{1,12} = 'PPG Filtered';
                    signalNameArray{1,13} = 'Heart Rate';
                    signalFormatArray{1,12} = 'CAL';
                    signalFormatArray{1,13} = 'CAL';
                    signalUnitArray{1,12} = 'mV';
                    signalUnitArray{1,13} = 'BPM';

                    firsttime = writeHeadersToFile(fileName,signalNameArray,signalFormatArray,signalUnitArray);
                end

                if ~isempty(newData)                                                            % TRUE if new data has arrived
                    % get PPG signal indicex
                    chIndexT = find(ismember(signalNameArray, 'Time Stamp'));
                    chIndexP = find(ismember(signalNameArray, ['Internal ADC A' num2str(PPGChannelNum)]));   % PPG data output             
                    PPGData = newData(:,chIndexP);
                    PPGDataFiltered = PPGData;
                    PPGDataFiltered = lpfPPG.filterData(PPGDataFiltered);                       % filter with low pass filter
                    %newData(:,chIndexP)=PPGDataFiltered;
                    newheartRate = PPG2HR.ppgToHrConversion(PPGDataFiltered, newData(:,chIndexT));                   % compute Heart Rate from PPG data
                    %newData = [newData PPGDataFiltered newheartRate];
                    newData = [newData PPGDataFiltered newheartRate];
                    %dlmwrite(fileName, newData, '-append', 'delimiter', '\t','precision',16); % Append the new data to the file in a tab delimited format   

                    plotData = [plotData; newData];                                             % update the plotDataBuffer with the new PPG data
                    %filteredplotData = [filteredplotData; PPGDataFiltered];                     % update the filteredplotData buffer with the new filtered PPG data
                    %heartRate = [heartRate; newheartRate];                                      % update the filteredHRData buffer with the new filtered Heart Rate data
                    numPlotSamples = size(plotData,1);                          
                    numSamples = numSamples + size(newData,1);
                    timeStampNew = newData(:,chIndexT);                                       % get timestamps
                    timeStamp = [timeStamp; timeStampNew];

    %                 newstoreData = [timeStampNew PPGData PPGDataFiltered newheartRate];
    %                 storeData = [storeData; newstoreData];


                      %Chunk of data being send to LSL
                    outlet.push_chunk(newData');
    %                 dlmwrite(fileName, storeData, '-append', 'delimiter', '\t','precision',16);                % append the new data to the file in a tab delimited format
                    dlmwrite(fileName, newData, '-append', 'delimiter', '\t','precision',16); % Append the new data to the file in a tab delimited format   

                     if numSamples > NO_SAMPLES_IN_PLOT

                            plotData = plotData(numPlotSamples-NO_SAMPLES_IN_PLOT+1:end,:);
                            %filteredplotData = filteredplotData(numPlotSamples-NO_SAMPLES_IN_PLOT+1:end,:);
                            %heartRate = heartRate(numPlotSamples-NO_SAMPLES_IN_PLOT+1:end,:);

                     end
                     sampleNumber = max(numSamples-NO_SAMPLES_IN_PLOT+1,1):numSamples;

                     chIndex(1) = 13; %find(ismember(signalNameArray, 'PPG Filtered'));   % get signal indices
                     if (shimmer.ShimmerVersion == 3)                              % for Shimmer3
                         chIndex(2) = find(ismember(signalNameArray, 'Low Noise Accelerometer X'));
                         chIndex(3) = find(ismember(signalNameArray, 'Low Noise Accelerometer Y'));
                         chIndex(4) = find(ismember(signalNameArray, 'Low Noise Accelerometer Z'));
                     elseif (shimmer.ShimmerVersion < 3)                           % for Shimmer2/2r
                         chIndex(2) = find(ismember(signalNameArray, 'Accelerometer X'));
                         chIndex(3) = find(ismember(signalNameArray, 'Accelerometer Y'));
                         chIndex(4) = find(ismember(signalNameArray, 'Accelerometer Z'));
                     end
                     chIndex(5) = find(ismember(signalNameArray, 'Gyroscope X'));
                     chIndex(6) = find(ismember(signalNameArray, 'Gyroscope Y'));
                     chIndex(7) = find(ismember(signalNameArray, 'Gyroscope Z'));
                     chIndex(8) = find(ismember(signalNameArray, 'Magnetometer X'));
                     chIndex(9) = find(ismember(signalNameArray, 'Magnetometer Y'));
                     chIndex(10) = find(ismember(signalNameArray, 'Magnetometer Z'));

                    % plotting the data

                    set(0,'CurrentFigure',h.figure1);           
                    subplot(2,2,1);                                            % Create subplot
                    signalIndex = chIndex(1);
                    plot(sampleNumber, plotData(:,signalIndex));               % Plot the time stamp data
                    legend('Heart rate');  %legend('PPG (mV)');   
                    xlim([sampleNumber(1) sampleNumber(end)]);
                    ylim('auto'); 

                    subplot(2,2,2);                                            % Create subplot
                    signalIndex1 = chIndex(2);
                    signalIndex2 = chIndex(3);
                    signalIndex3 = chIndex(4);
                    plot(sampleNumber, plotData(:,[signalIndex1 signalIndex2 signalIndex3]));                                 % Plot the accelerometer data
                    legendName1=[signalFormatArray{signalIndex1} ' ' signalNameArray{signalIndex1} ' (' signalUnitArray{signalIndex1} ')'];  
                    legendName2=[signalFormatArray{signalIndex2} ' ' signalNameArray{signalIndex2} ' (' signalUnitArray{signalIndex2} ')'];  
                    legendName3=[signalFormatArray{signalIndex3} ' ' signalNameArray{signalIndex3} ' (' signalUnitArray{signalIndex3} ')'];  
                    legend(legendName1,legendName2,legendName3); % Add legend to plot
                    xlim([sampleNumber(1) sampleNumber(end)]);

                    subplot(2,2,3);                                            % Create subplot
                    signalIndex1 = chIndex(5);
                    signalIndex2 = chIndex(6);
                    signalIndex3 = chIndex(7);
                    plot(sampleNumber, plotData(:,[signalIndex1 signalIndex2 signalIndex3]));                                 % Plot the gyroscope data
                    legendName1=[signalFormatArray{signalIndex1} ' ' signalNameArray{signalIndex1} ' (' signalUnitArray{signalIndex1} ')'];  
                    legendName2=[signalFormatArray{signalIndex2} ' ' signalNameArray{signalIndex2} ' (' signalUnitArray{signalIndex2} ')'];  
                    legendName3=[signalFormatArray{signalIndex3} ' ' signalNameArray{signalIndex3} ' (' signalUnitArray{signalIndex3} ')'];  
                    legend(legendName1,legendName2,legendName3); % Add legend to plot
                    xlim([sampleNumber(1) sampleNumber(end)]);


                    subplot(2,2,4);                                            % Create subplot
                    signalIndex1 = chIndex(8);
                    signalIndex2 = chIndex(9);
                    signalIndex3 = chIndex(10);
                    plot(sampleNumber, plotData(:,[signalIndex1 signalIndex2 signalIndex3]));                                 % Plot the magnetometer data
                    legendName1=[signalFormatArray{signalIndex1} ' ' signalNameArray{signalIndex1} ' (' signalUnitArray{signalIndex1} ')'];  
                    legendName2=[signalFormatArray{signalIndex2} ' ' signalNameArray{signalIndex2} ' (' signalUnitArray{signalIndex2} ')'];  
                    legendName3=[signalFormatArray{signalIndex3} ' ' signalNameArray{signalIndex3} ' (' signalUnitArray{signalIndex3} ')'];  
                    legend(legendName1,legendName2,legendName3); % Add legend to plot
                    xlim([sampleNumber(1) sampleNumber(end)]);


    %                 set(0,'CurrentFigure',h.figure1);         
    %                 subplot(3,1,1)
    %                 plot(sampleNumber, plotData(:,1));                         % plot the PPG data
    %                 legend('PPG (mV)'); 
    %                 xlim([sampleNumber(1) sampleNumber(end)]);
    %                 ylim('auto');                                          
    %                 
    %                 subplot(3,1,2)
    %                 plot(sampleNumber, filteredplotData(:,1));                 % plot the filtered PPG data
    %                 legend('Filtered PPG (mV)'); 
    %                 xlim([sampleNumber(1) sampleNumber(end)]);
    %                 ylim('auto');                                          
    %                 
    %                 subplot(3,1,3)
    %                 plot(sampleNumber, heartRate);                             % plot the Heart Rate data
    %                 legend('Heart Rate (BPM');   
    %                 xlim([sampleNumber(1) sampleNumber(end)]);
    %                 ylim('auto');           

                end

                elapsedTime = elapsedTime + toc;                               % stop timer and add to elapsed time
                tic;                                                           % start timer           
            else
                break;
            end    
        end  
        
        elapsedTime = elapsedTime + toc;                                   % stop timer
        fprintf('The percentage of received packets: %d \n',shimmer.getpercentageofpacketsreceived(timeStamp)); % Detect loss packets
        shimmer.stop;                                                      % stop data streaming                                                    
       
    end 
    
    
    shimmer.disconnect;                                                    % disconnect from shimmer
        
end


