# Part of this code was obtained from https://brainflow.readthedocs.io/en/stable/Examples.html
# and from https://github.com/openbci-archive/OpenBCI_MNE/blob/master/python/brainflow_to_mne.py
# This is to connect to BrainBit head band to LSL

# Units of Measure
# For EEG, BrainFlow returns uV

# Mode of work
# At this moment, it records data over ~5 seconds from BrainBit and save it to a csv file
# I need to find a way of recording data in a continuous way (may take idea from the badge code),
# plot it in real time, and find a way to connect it to LSL

import argparse
import time
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd


import brainflow
from brainflow.board_shim import BoardShim, BrainFlowInputParams, BoardIds
from brainflow.data_filter import DataFilter, FilterTypes, AggOperations

#from mne.viz.topomap import _prepare_topo_plot, plot_topomap
import mne
# from mne.channels import read_layout

from mpl_toolkits.axes_grid1 import make_axes_locatable
# from networkx.drawing.tests.test_pylab import plt


def main():
    BoardShim.enable_dev_board_logger()
    # use BrainBit board
    params = BrainFlowInputParams()
    # Specifying the use of BrainBit
    board = BoardShim(BoardIds.BRAINBIT_BOARD.value, params)
    board.prepare_session()
    board.start_stream()
    time.sleep(15)
    data = board.get_board_data()
    board.stop_stream()
    board.release_session()

    # print(data)

    eeg_channels = BoardShim.get_eeg_channels(BoardIds.BRAINBIT_BOARD.value)
    print(eeg_channels)
    eeg_data = data[eeg_channels, :]
    eeg_data = eeg_data / 1000000  # Converting uV to V

    DataFilter.write_file(data, 'test.csv', 'w')
    restored_data = DataFilter.read_file('test.csv')
    restored_df = pd.DataFrame(np.transpose(restored_data))
    print('Data From the File')
    print(restored_df.head(10))

    # Creating MNE objects from brainflow data arrays
    ch_types = ['eeg', 'eeg', 'eeg', 'eeg']
    ch_names = ['T3', 'T4', 'O1', 'O2']
    sfreq = BoardShim.get_sampling_rate(BoardIds.BRAINBIT_BOARD.value)
    info = mne.create_info(ch_names=ch_names, sfreq=sfreq, ch_types=ch_types)
    raw = mne.io.RawArray(eeg_data, info)
    # its time to plot something!
    raw.plot_psd(average=True)
    plt.savefig('brainbit_test.png')

if __name__ == "__main__":
    main()