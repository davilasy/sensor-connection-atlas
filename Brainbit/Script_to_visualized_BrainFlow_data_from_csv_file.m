%Script to plot BrainBit data collected using Python BrainFlow package
%Sampling frequency = 250 Hz

test_data = test(600:end,2:5)./1000000;
figure; plot(test_data(:,1));hold on;plot(test_data(:,2));hold on;plot(test_data(:,3)); hold on;plot(test_data(:,4))

%Correcting drift of the channels' data
%Channel 1
figure;
subplot(4,1,1)
Ch1 = test_data(:,1);
MCh1 = movmean(Ch1,8);
Clean_Ch1 = Ch1 - MCh1;
plot(Clean_Ch1)

%Channel 2
subplot(4,1,2)
Ch2 = test_data(:,2);
MCh2 = movmean(Ch2,8);
Clean_Ch2 = Ch2 - MCh2;
plot(Clean_Ch2)

%Channel 3
subplot(4,1,3)
Ch3 = test_data(:,3);
MCh3 = movmean(Ch3,8);
Clean_Ch3 = Ch3 - MCh3;
plot(Clean_Ch3)

%Channel 4
subplot(4,1,4)
Ch4 = test_data(:,4);
MCh4 = movmean(Ch4,8);
Clean_Ch4 = Ch4 - MCh4;
plot(Clean_Ch4)