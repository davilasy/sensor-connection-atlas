Material in this repository is intended to be used to record audio and physiological data from multiple units simultaneously. 

The following steps should be taken to set up the environment for data collection:

**LSL environment:**
1) Download Visual Studio Build tools: https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=BuildTools&rel=16
2) Create a folder and save the LSL recorder application folder and the audio application folder

**Connection of BrainBit with LSL:**
1) Turn computer Bluetooth ON and add BrainBit device
2) Install python 3.7
3) Install PyCharm and create a project using the folder were the source code is located. Or install GitBash, use ls and cd to navigate to the folder with the ".yaml" file to connect BrainBit.
4) Install timeflux package using "pip install timeflux==0.6.1", "pip install timeflux-brainflow==0.4.0", and "pip install timeflux-ui"
Note: if timeflux has been already been installed using "pip install timeflux" make sure that the package version is 0.6.1 by typing "pip list". Otherwise, uninstall the package using "pip uninstall timeflux" and installing it as indicating above.
5) Make sure the device is connected via Bluetooth to the computer
6) Run the program by writing "timeflux brainbit_LSL_connection.yaml" in the terminal of PyCharm

**Connection of Shimmer with LSL:**
1) Folder: .../Shimmer-matlab/Shimmer-matlab app with plots
    - The "Shimmer-matlab app with plots" contains the executable for an application that shows in real time the collection of data
    - The "Shimmer-matlab app without plots" executable connects the Shimmer to LSL but do not show data plots
2) Requirements: Computer with Windows 10 64-bit operating system
3) Install Realterm Serial Terminal (https://sourceforge.net/projects/realterm/files/Realterm/2.0.0.57/) 
4) Install MATLAB Runtime (R2019b): https://www.mathworks.com/products/compiler/matlab-runtime.html 
5) Add the Shimmer device to the Bluetooth devices being recognized by the computer
6) Run the Shimmer MATLAB application, add the Shimmer ID, add the COMP port, and click Run

**Recording from LSL:**
1) Open LabRecorder.exe
2) Open and link any other LSL application of interest (such as the audio recorder)
3) Make sure you save the file to be recorded in a desire location
4) Start recording
5) LSL github: https://github.com/labstreaminglayer

Cite this work as follows: S. Dávila-Montero, S. Parsnejad, E. Ashoori, D. Goderis, and A. J. Mason, "Design of a Multi-Sensor Framework for the Real-time Monitoring of Social Interactions," IEEE International Symposium in Circuits and Systems (ISCAS), 2022.

**Problems:** MSU EGR Brainbit and LSL connection
- LSL apps cannot make data streams visible to other computers because of windows firewalls and antivirus problems
- Brainbit-python code shows the following warning/error: "stream_outlet_impl.cpp:86    WARN| Couldn't create multicast responder for 224.0.0.1 (set_option: A socket operation was attempted to an unreachable host)"
- The following blog contains an explaination that leads to a network issue (arguably related to the firewalls and anti-virus in my case): https://stackoverflow.com/questions/5164466/a-socket-operation-was-attempted-to-an-unreachable-host 

**Solutions:** 
- To ensure connection between Brainbit and LSL, its code should be run with an specific timeflux python package version. 
- To allow computers to see each other, all should be connected to a local wifi router:
- Then, in addition, the fllowing should be made:
    - As an administrator, got to Firewall & network protection
    - Then go to: Allow an app through firewall -> "Change settings" -> "Allow another app"
    - Look for: LSL recorder, LSL audio app, Shimmer matlab app or apps, python, and gitlab

**NOTE:** To connect multiple devices to the same computer, bluetooth 5.0 is highly recommended. 
